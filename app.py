from flask import Flask
from redis import Redis
from os import environ

host_run = environ.get('HOST_RUN', '0.0.0.0')
debug = environ.get('DEBUG', True)
host_redis = environ.get('HOST_REDIS', 'redis')
port_redis = environ.get('PORT_REDIS', 6379)

app = Flask(__name__)
redis = Redis(host=host_redis, port=port_redis)


@app.route('/')
def hello():
    redis.incr('hits')
    return 'Hello World! {} times.'.format(redis.get('hits'))


if __name__ == "__main__":
    app.run(host=host_run, debug=debug)
